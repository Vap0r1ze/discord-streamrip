const { existsSync, rmSync } = require('fs')
const { join } = require('path')
const esbuild = require('esbuild')

const NODE_VERSION = '14'
const CHROME_VERSION = '91'

const BUILD_DIR = join(__dirname, '../dist')

const COMMON_OPTIONS = {
  absWorkingDir: process.cwd(),
  minify: true,
  bundle: true,
  sourcemap: 'inline',
}
const BUILD_OPTIONS = [
  {
    ...COMMON_OPTIONS,
    entryPoints: ['./src/main'],
    format: 'cjs',
    platform: 'node',
    target: 'node' + NODE_VERSION,
    outfile: 'dist/index.js',
    external: ['puppeteer', 'superagent'],
  },
  {
    ...COMMON_OPTIONS,
    entryPoints: ['./src/renderer'],
    format: 'iife',
    platform: 'browser',
    target: 'chrome' + CHROME_VERSION,
    outfile: 'dist/preload.js',
  },
]

async function build() {
  const buildStart = process.hrtime.bigint()

  // if (existsSync(BUILD_DIR)) {
  //   rmSync(join(BUILD_DIR, 'index.js'), { recursive: true })
  //   rmSync(join(BUILD_DIR, 'preload.js'), { recursive: true })
  // }

  return Promise.all(
    BUILD_OPTIONS.map(async (options) => esbuild.build(options)),
  ).then((results) => {
    const durationMs = Number(process.hrtime.bigint() - buildStart) / 1e6
    return [results, durationMs]
  })
}

if (require.main === module) {
  build()
    .then(([, buildMs]) => {
      console.log(`Done! Built in ${buildMs.toFixed(3)}ms`)
    })
    .catch((error) => {
      console.error('Could not build:', error)
    })
}

module.exports = build
