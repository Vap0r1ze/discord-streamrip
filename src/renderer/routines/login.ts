export default function login(token: string) {
  if (!token) return

  const tokens: Record<string, string> = JSON.parse(
    localStorage.getItem('tokens') ?? '{}',
  )
  const userId = atob(token.slice(0, token.indexOf('.')))
  tokens[userId] = token
  localStorage.setItem('tokens', JSON.stringify(tokens))
  localStorage.setItem('token', JSON.stringify(token))
}
