export default class MediaRecording {
  _recorder: MediaRecorder
  blobs: Blob[] = []
  dataHandlerQueue: MediaRecorder['ondataavailable'][] = []

  static get defaultOpts(): MediaRecorderOptions | null {
    return null
    // return {
    //   mimeType: 'video/webm;codecs:vp8,opus'
    // }
  }

  constructor(stream: MediaStream, opts = MediaRecording.defaultOpts) {
    this._recorder = new MediaRecorder(stream, opts)
  }
  get id() {
    return this._recorder.stream.id
  }
  start(): void {
    this._recorder.start()
    this._recorder.ondataavailable = (event) => {
      const handler = this.dataHandlerQueue.shift()
      if (handler) handler.call(this, event)
      else this.blobs.push(event.data)
    }
  }
  stop(): Promise<void> {
    if (this._recorder.state === 'inactive') return Promise.resolve()
    return new Promise((resolve) => {
      this.dataHandlerQueue.push((event) => {
        this.blobs.push(event.data)
        resolve()
      })
      this._recorder.stop()
    })
  }
  consume(): Promise<Blob> {
    if (this._recorder.state === 'inactive') {
      return Promise.resolve(this.consumeCache())
    }
    return new Promise((resolve) => {
      this.dataHandlerQueue.push((event) => {
        this.blobs.push(event.data)
        resolve(this.consumeCache())
      })
      this._recorder.requestData()
    })
  }
  consumeCache(): Blob {
    const blob = new Blob(this.blobs)
    this.blobs = []
    return blob
  }
}
