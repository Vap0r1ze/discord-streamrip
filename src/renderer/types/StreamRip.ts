import MediaEngineManager from '../services/MediaEngineManager'
import ModuleManager from '../services/ModuleManager'
import WriteManager from '../services/WriteManager'
import RecordingManager from '../services/RecordingManager'

export type StreamRipOptions = {
  writeInterval: number
}

export default class StreamRip {
  modules = new ModuleManager()
  recorder: RecordingManager
  mediaEngine: MediaEngineManager
  writer: WriteManager

  constructor(opts: StreamRipOptions) {
    // Initialize Modules
    this.modules.on('foundAll', (modules) => {
      const mediaEngine = modules.mediaEngineStore.getMediaEngine()
      this.mediaEngine = new MediaEngineManager(mediaEngine)
      this.recorder = new RecordingManager(modules.videoStreamStore)
      this.writer = new WriteManager({
        recordingManager: this.recorder,
        consumeRate: opts.writeInterval,
      })
      this.init()
    })
  }
  init() {
    this.mediaEngine.on('stream', (userId, streamId) => {
      this.recorder.start(userId, streamId)
    })
    this.mediaEngine.on('streamEnd', (userId) => {
      this.recorder.stop(userId)
    })
  }

  onModuleLoad(module: any) {
    this.modules.checkModule(module)
  }
}
