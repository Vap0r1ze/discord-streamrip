import login from './routines/login'
import { InstanceName } from '../constants'
import StreamRip from './types/StreamRip'
import config from '../../config.json'

console.log(config)
login(config.token)

const ripper = new StreamRip({
  writeInterval: 5000, // 5s
})

Object.defineProperty(window, InstanceName, {
  value: ripper,
})

window.addEventListener('beforeunload', (event) => {
  if (Object.keys(ripper.recorder.recordings).length) {
    event.preventDefault()
    return (event.returnValue = 'There are unsaved recording chunks')
  }
})
