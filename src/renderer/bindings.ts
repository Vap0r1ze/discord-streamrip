import type { RecordedChunks } from '../common'

declare global {
  interface Window {
    writeChunks(chunks: RecordedChunks): Promise<void>
  }
}

export const writeChunks = window.writeChunks
