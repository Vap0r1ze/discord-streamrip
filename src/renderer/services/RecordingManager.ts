import type { RecordedChunks } from '../../common'
import type { VideoStreamStore } from './ModuleManager'
import MediaRecording from '../types/MediaRecording'

function blobBtoa(blob: Blob): Promise<string> {
  return new Promise((resolve) => {
    const reader = new FileReader()
    reader.onloadend = () => resolve(<string>reader.result)
    reader.readAsDataURL(blob)
  })
}

export default class RecordingManager {
  recordings: Record<string, MediaRecording[]> = {}
  videoStreamStore: VideoStreamStore

  constructor(videoStreamStore: VideoStreamStore) {
    this.videoStreamStore = videoStreamStore
  }

  start(userId: string, streamId: string) {
    this.recordings[userId] ??= []

    const origStream = this.videoStreamStore.get(streamId)
    const stream = new MediaStream()

    const videoTracks = origStream.getVideoTracks()
    const audioTracks = origStream.getAudioTracks()
    videoTracks.forEach((track) => stream.addTrack(track))
    audioTracks.forEach((track) => stream.addTrack(track))

    const recording = new MediaRecording(stream)
    recording.start()
    this.recordings[userId].push(recording)
  }
  stop(userId: string): Promise<void> {
    if (!this.recordings[userId]) return Promise.resolve()
    return Promise.all(
      this.recordings[userId].map((recording) => recording.stop()),
    ).then()
  }
  clean() {
    for (const [userId, recordings] of Object.entries(this.recordings)) {
      this.recordings[userId] = recordings.filter(
        (recording) =>
          recording._recorder.state !== 'inactive' || recording.blobs.length,
      )
      if (!this.recordings[userId].length) delete this.recordings[userId]
    }
  }
  abort(): Promise<void> {
    return Promise.all(
      Object.keys(this.recordings).map((userId) => this.stop(userId)),
    ).then()
  }
  async consume(): Promise<RecordedChunks | null> {
    const chunks: RecordedChunks = {}
    const chunkTransforms: Promise<void>[] = []

    for (const [userId, recordings] of Object.entries(this.recordings)) {
      for (const recording of recordings) {
        const chunk = await recording.consume()
        if (!chunk?.size) continue
        chunks[userId] ??= {}
        const promise = blobBtoa(chunk).then((data) => {
          chunks[userId][recording.id] = data
        })
        chunkTransforms.push(promise)
      }
    }

    this.clean()

    console.time('Blob btoa')
    await Promise.all(chunkTransforms)
    console.timeEnd('Blob btoa')

    return chunkTransforms.length ? chunks : null
  }
}
