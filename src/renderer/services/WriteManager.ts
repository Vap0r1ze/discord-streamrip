import type RecordingManager from './RecordingManager'
import { writeChunks } from '../bindings'

export type WriteManagerOptions = {
  consumeRate: number
  recordingManager: RecordingManager
}

export default class WriteManager {
  recorder: RecordingManager
  consumeRate: number
  consumeRoutine: ReturnType<typeof setTimeout>

  constructor(opts: WriteManagerOptions) {
    this.recorder = opts.recordingManager
    this.consumeRate = opts.consumeRate
    this.consumeRoutine = setTimeout(() => {
      this.doConsume()
    }, opts.consumeRate)
  }

  async doConsume() {
    this.consumeRoutine = null
    const chunks = await this.recorder.consume()
    if (chunks) await writeChunks(chunks)
    this.consumeRoutine = setTimeout(
      this.doConsume.bind(this),
      this.consumeRate,
    )
  }
}
