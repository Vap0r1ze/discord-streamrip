import { EventEmitter } from 'eventemitter3'

export type VideoStreamStore = {
  get(id: string): MediaStream
  set(stream: MediaStream): string
  del(id: string): Boolean
}
export type MediaEngineStore = {
  getMediaEngine(): MediaEngine
}

export type MediaEngine = {
  connect: (...args: any[]) => any
}

export const enum ModuleType {
  VideoStreamStore = 'VIDEO_STREAM_STORE',
  MediaEngineStore = 'MEDIA_ENGINE_STORE',
}

export type ModuleFilter = (exports: any) => any
export const moduleFilters: Record<ModuleType, ModuleFilter> = {
  [ModuleType.VideoStreamStore]: (exports) => {
    const keys = Object.keys(Object.getOwnPropertyDescriptors(exports))
    if (keys.length !== 3) return
    if (typeof exports[keys[0]] !== 'function') return
    if (!exports[keys[0]].toString().includes('uniqueId("VideoStream")')) return
    return keys
  },
  [ModuleType.MediaEngineStore]: (exports) => {
    return !!exports?.Z?.getMediaEngine
  },
}

function getVideoStreamStore(module: any, keys: string[]): VideoStreamStore {
  const [setKey, delKey, getKey] = keys
  return {
    set: module.exports[setKey],
    del: module.exports[delKey],
    get: module.exports[getKey],
  }
}

class ModuleManager extends EventEmitter<{
  foundAll: [Required<ModuleManager['modules']>]
}> {
  modules: {
    videoStreamStore?: VideoStreamStore
    mediaEngineStore?: MediaEngineStore
  } = {
    videoStreamStore: null,
    mediaEngineStore: null,
  }

  get hasFoundAll() {
    return Object.values(this.modules).every(Boolean)
  }

  checkModule(module: any) {
    if (this.hasFoundAll) return
    if (!module.exports) return

    for (const [type, filter] of Object.entries(moduleFilters)) {
      const data = filter(module.exports)
      if (!data) continue
      this.onModuleRecognized(<ModuleType>(<unknown>type), module, data)
    }
  }
  onModuleRecognized(name: ModuleType, module: any, data: any) {
    switch (name) {
      case ModuleType.VideoStreamStore: {
        this.modules.videoStreamStore = getVideoStreamStore(module, data)
        break
      }
      case ModuleType.MediaEngineStore: {
        this.modules.mediaEngineStore = module.exports.Z
        // this.patchConnect()
        // this.mediaEngine = this.mediaEngineStore.getMediaEngine()
        // this.mediaEngine.on('video', (...args) => {
        //   console.log('omg there is video HAPPENIENG', args)
        // })
        break
      }
    }

    if (this.hasFoundAll)
      this.emit('foundAll', {
        videoStreamStore: this.modules.videoStreamStore!,
        mediaEngineStore: this.modules.mediaEngineStore!,
      })
  }
}

export default ModuleManager
