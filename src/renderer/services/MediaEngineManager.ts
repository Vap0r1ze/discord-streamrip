import type { MediaEngine } from './ModuleManager'
import { EventEmitter } from 'eventemitter3'

const kConnectPatched = Symbol('patched connect')

export default class MediaEngineManager extends EventEmitter<{
  stream: [userId: string, streamId: string]
  streamEnd: [userId: string]
}> {
  mediaEngine: MediaEngine

  videoDefers: Record<string, number> = {}

  constructor(mediaEngine: MediaEngine) {
    super()
    this.mediaEngine = mediaEngine
    this.patchConnect()
  }

  patchConnect() {
    const proto = this.mediaEngine.constructor.prototype

    if (proto[kConnectPatched]) return
    proto[kConnectPatched] = true

    proto.connect = new Proxy(proto.connect, {
      apply: (connect, mediaEngine, args) => {
        const connection = connect.apply(mediaEngine, args)
        connection.on('video', this.onConnVideo.bind(this))
        return connection
      },
    })
  }
  onConnVideo(...[userId, streamId, , , , videoStreamParameters]: string[]) {
    console.log('Connection Video Event:', {
      userId,
      streamId,
      videoStreamParameters,
    })
    if (streamId && !videoStreamParameters) {
      // new MediaStream
      clearTimeout(this.videoDefers[userId])
      this.videoDefers[userId] = setTimeout(() => {
        delete this.videoDefers[userId]
        this.emit('stream', userId, streamId)
      })
    } else if (!streamId && !videoStreamParameters) {
      // stream end
      this.emit('streamEnd', userId)
    }
  }
}
