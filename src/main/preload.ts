import fs from 'node:fs/promises'
import { join } from 'node:path'

type PageFunction = () => unknown

export default async function getPreload() {
  const file = await fs.readFile(join(__dirname, 'preload.js'))
  return new Function(file.toString('utf-8')) as PageFunction
}
