import type { RecordedChunks } from '../common'
import type { Page } from 'puppeteer'
import fs from 'node:fs/promises'
import { join } from 'node:path'
import { save_folder } from '../../config.json'

const savePath = join(process.cwd(), save_folder)

export async function writeChunks(chunks: RecordedChunks) {
  const appends: Promise<void>[] = []

  await fs.mkdir(savePath, { recursive: true })
  console.time('Write')
  for (const [userId, chunkMap] of Object.entries(chunks)) {
    await fs.mkdir(join(savePath, userId), { recursive: true })
    for (const [chunkId, chunkUrl] of Object.entries(chunkMap)) {
      const chunkData = Buffer.from(
        chunkUrl.slice(chunkUrl.indexOf(',') + 1),
        'base64',
      )
      appends.push(
        fs.appendFile(join('user_video', userId, `${chunkId}.webm`), chunkData),
      )
      console.log(
        `[${new Date().toISOString()}] Wrote chunk ${userId}/${chunkId}`,
      )
    }
  }
  console.timeEnd('Write')
}

export default async function exposeBindings(page: Page) {
  await page.exposeFunction('writeChunks', writeChunks)
}
