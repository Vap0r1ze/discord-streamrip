import type { Browser, Page } from 'puppeteer'

export async function closeOtherPages(browser: Browser, pages: Page[]) {
  const openPages = await browser.pages()
  const closes = openPages.map((page) => !pages.includes(page) && page.close())
  await Promise.all(closes)
}
