import path from 'node:path'
import puppeteer from 'puppeteer'
import initIntercepts from './intercepts'
import exposeBindings from './bindings'
import getPreload from './preload'
import { closeOtherPages } from './util'
import { chromium_userdata, viewport } from '../../config.json'

export default async function start() {
  const browser = await puppeteer.launch({
    headless: false,
    userDataDir: path.join(__dirname, chromium_userdata),
    defaultViewport: { width: viewport[1], height: viewport[0] },
  })

  const page = await browser.newPage()

  await closeOtherPages(browser, [page])

  await initIntercepts(page)
  await exposeBindings(page)
  await page.evaluateOnNewDocument(await getPreload())

  await page.goto('https://discord.com/app')
}

if (require.main === module) {
  start().catch((error) => {
    console.error('Main threw:', error)
  })
}
