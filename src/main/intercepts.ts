import type { HTTPRequest, Page } from 'puppeteer'
import superagent from 'superagent'
import { InstanceName, WebpackSignatures } from '../constants'

let WEBPACK: {
  path: string
  code?: string
} | null = null

const Intercepts = {
  Webpack: async (url: string) => {
    const { body } = await superagent.get(url)
    const code = patchWebpack(body.toString('utf-8'))

    return {
      contentType: 'application/javascript',
      body: code,
    }
  },
  App: async (url: string) => {
    const { headers, text } = await superagent.get(url)

    return {
      headers: { ...headers },
      contentType: 'text/html',
      body: patchApp(text),
    }
  },
}

async function interceptRequest(page: Page, request: HTTPRequest) {
  const url = request.url()
  const isApp =
    request.isNavigationRequest() && new URL(url).host === 'discord.com'

  const respond = (response) => {
    console.log('Intercepted:', url)
    request.respond(response)
  }

  if (isApp) return respond(await Intercepts.App(url))
  else if (WEBPACK && url.endsWith(WEBPACK.path))
    return respond(await Intercepts.Webpack(url))

  return request.continue()
}

export default async function initIntercepts(page: Page) {
  await page.setRequestInterception(true)
  page.on('request', interceptRequest.bind(null, page))
}

function patchApp(document: string) {
  const scriptPath = document.match(WebpackSignatures.ScriptPath)?.[1]

  if (scriptPath) {
    WEBPACK = Object.assign({}, WEBPACK, {
      path: document.match(WebpackSignatures.ScriptPath)?.[1],
    })
  }

  return document.replace(WebpackSignatures.ScriptIntegrity, '')
}
function patchWebpack(code: string) {
  const requireFn =
    code[
      code.indexOf(WebpackSignatures.Require) + WebpackSignatures.Require.length
    ]
  const moduleVar = code[code.indexOf(WebpackSignatures.ModuleLoad) - 1]
  return code
    .replace(
      WebpackSignatures.AfterRequire,
      `${InstanceName}._webpack=${requireFn};`,
    )
    .replace(
      WebpackSignatures.ModuleLoad,
      WebpackSignatures.ModuleLoad +
        `${InstanceName}.onModuleLoad(${moduleVar});`,
    )
}
