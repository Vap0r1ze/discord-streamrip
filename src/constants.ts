export const InstanceName = 'streamRipper'

export const WebpackSignatures = {
  Cache: '={};',
  Require: 'function ',
  ModuleLoad: '.loaded=!0;',
  AfterRequire: /(?<=return \w\.exports\})/,
  ScriptIntegrity: /(?<=<script src="[^"]+") integrity="[^"]+"(?=>)/,
  ScriptPath: /<script src="([^"]+)"/,
}
