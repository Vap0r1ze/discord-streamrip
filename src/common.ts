export type RecordedChunks = {
  [userId: string]: {
    [streamId: string]: string
  }
}
