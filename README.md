# Requirements
- [NodeJS **v14**+](https://nodejs.org/)

# Setup
1. Run `npm ci` in the folder
2. Make a copy of `config.example.json` named `config.json`, and modify it to your needs
3. Run `npm run build` *after* you've configured
  - Note: **be aware that your token now also exists in the `dist` folder**

# How to Use
Run `npm start`, then just watch a stream on Discord

# Code Explanation
- the main code is in `src/main/index.ts`
- `src/renderer/index.ts` and all if its imports and bundled into one file, and that file is the first script ran on every page visited in the browser
- `src/config.ts` isnt really meant to be changed as a configuration file, but more just a placehold for a future config loader
